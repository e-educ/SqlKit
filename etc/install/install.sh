#!/bin/bash
echo
echo "--- Installation"

if [ $EUID -ne 0 ]; then
  echo "Lancer en root uniquement: # $0" 1>&2
  exit 1
fi

apt-get update
#apt-get upgrade
apt-get install python-pip python3-pip sqlitebrowser

pip install --upgrade pip
pip install --upgrade virtualenv

echo "--- "
echo "--- Fin Installation"
echo