#!/bin/bash
echo
echo "--- Installation de python env"

if [ $EUID -eq 0 ]; then
  echo "Ne pas lancer en root: # $0" 1>&2
  exit 1
fi

source common.conf

if [ "$USE_VIRTUALENV" == "yes" ]; then
	if [ ! -e $VIRTUALENV/bin/activate ]; then
		virtualenv  -p python3 $VIRTUALENV
		source $VIRTUALENV/bin/activate

		PIP="$(which pip3)"
		if [ "$USE_PIP_PROXY" == "yes" ]; then
		PIP="$(which pip3) --proxy $HTTP_PROXY"
		fi

		$PIP install --upgrade pip
		$PIP install -r $BASE/requirements.txt
		$PIP install PyQt5
	else
		source $VIRTUALENV/bin/activate
	fi
fi

source make-config-file.conf

echo "--- Fin d'installation "
echo