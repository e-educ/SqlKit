# encoding: utf-8
# setting configuration and global variables
#
database_conf = {
    'default': 'sqlite',
    'sqlite': 'etc/sql/sqlkit.sqlite',
    'mysql': {
        'dbname': 'sqlkit',
        'user': 'sqlkit',
        'passwd': 'sqlkit',
        'host': '127.0.0.1',
    }
}

#