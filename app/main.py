# encoding: utf-8
# main sqlkit
# Created on 28 mars 2018
# @author: denis@e-educ.fr
#
import random
from faker import Faker
from app.settings import database_conf
from app.models import Classe, Matiere, Eleve, Notes

class DemoSqlkit():

    def start(self):
        self.faker = Faker('fr_FR')
        print("[ Start ] ----------------------------")
        print("    Configuration", database_conf)


    """
    Ajouter un enregistrement
        objet.add(nom='xxxx', prenom='yyy', ...)
    """
    def gen_matiere(self):
        noms = ['MATHS', 'PHYSIQUE', 'ANGLAIS']
        sujet = Matiere()

        print("[ gen_matiere ] ----------------------------")
        for n in noms:
            print("    Création de: ", n)
            try:
                sujet.add(nom=n)
            except:
                pass

    def gen_classe(self):
        noms = [('2A', 1), ('2B', 10), ('1A', 12), ('1B', 20), ('TA', 22), ('TB', 30) ]
        classe = Classe()

        print("[ gen_classe ] ----------------------------")
        for n, i in noms:
            print("    Création de: ", n, i)
            try:
                classe.add(nom=n, salle=i)
            except:
                pass

    def gen_eleve(self):
        print("[ gen_eleve ] ----------------------------")
        eleve = Eleve()
        for _ in range(8):
            cid = random.randrange(1,9)
            nom = self.faker.last_name_male()
            print("    Création de: ", cid, nom)
            eleve.add(classe_id=cid, nom=nom)

    def gen_notes(self):
        notes = Notes()
        eleves = Eleve().all()
        print("[ gen_notes ] ----------------------------")
        for _ in range(5):
            for eleve in eleves:
                eid = eleve.get('id')
                mid = random.randrange(1,4)
                note = random.randrange(0,21)
                print("    Création de: ", eid, mid, note, eleve)
                notes.add(eleve_id=eid, matiere_id=mid, note=note)

    def selects(self):
        eleve = Eleve()
        print("[ selects ] ----------------------------")
        r = eleve.get_by_id(5)
        print("    Sélection eleve numero: 5", r)

        r = eleve.get_by_id(4)
        nom = r.get('nom')
        r = eleve.query("""
            select * from eleve where nom='%s'
        """% (nom))
        print("    Sélection eleve nom: %s"% nom, r)

        print("[ Sélection avec jointure sur classe] -------------------")
        r = eleve.query("""
            select e.*, c.nom as nom_classe
              from eleve as e
                join classe as c on c.id=e.classe_id
        """)
        for e in r:
            print("    ", e)

        print("[ Sélection avec jointure sur notes et calculs ] -------------------")
        r = eleve.query("""
            select
              n.date,
              e.*,
              c.nom as nom_classe,
              m.nom as matiere,
              sum(n.note) as total_matiere,
              count(n.matiere_id) as nb,
              AVG(n.note) as moyenne

              from eleve as e
                join classe as c on c.id=e.classe_id
                join notes as n on n.eleve_id=e.id
                join matiere as m on n.matiere_id=m.id
            group by n.eleve_id, n.matiere_id
        """)

        for e in r:
            total_matiere = e.get('total_matiere')
            nb = e.get('nb')
            print ('    %s nom: %s classe: %s total: %s %s nb: %s [ moyenne calculée: %s,  moyenne: %s ]' % (
                e.get('date'), e.get('nom'), e.get('nom_classe'), e.get('matiere'),
                total_matiere, nb, round(total_matiere/nb, 2), e.get('moyenne'),
            ))

    def update_classe(self):
        pass








