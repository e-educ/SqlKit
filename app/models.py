# encoding: utf-8
#     models
#
#     @author: denis@e-educ.fr
from sqlkit.abstract import DBCore



class Matiere(DBCore):
    _TABLE = 'matiere'

    _CREATE = {
        'sqlite': [
            "id INTEGER PRIMARY KEY AUTOINCREMENT",
            "nom TEXT NOT NULL UNIQUE",

        ],
        'mysql': [
            "id INT NOT NULL AUTO_INCREMENT PRIMARY KEY",
            "nom VARCHAR(64) NOT NULL",
            "UNIQUE nom",
        ]
    }

class Classe(DBCore):
    _TABLE = 'classe'
    _CREATE = {
        'sqlite': [
            "id INTEGER PRIMARY KEY AUTOINCREMENT",
            "nom TEXT NOT NULL UNIQUE",
            "salle INTEGER DEFAULT NULL",
        ],
        'mysql': [
            "id INT NOT NULL AUTO_INCREMENT PRIMARY KEY",
            "nom VARCHAR(64) NOT NULL",
            "salle INT(2) DEFAULT NULL",
            "UNIQUE nom",
        ]
    }


class Eleve(DBCore):
    _TABLE = 'eleve'
    _CREATE = {
        'sqlite' : [
            "id INTEGER PRIMARY KEY AUTOINCREMENT",
            "classe_id INTEGER DEFAULT NULL",
            "nom TEXT DEFAULT NULL",
            "FOREIGN KEY(classe_id) REFERENCES classe(id)",
        ],
        'mysql': [
            "id INT NOT NULL AUTO_INCREMENT PRIMARY KEY",
            "classe_id INT DEFAULT NULL",
            "nom VARCHAR(64) DEFAULT NULL",
            "FOREIGN KEY(classe_id) REFERENCES classe(id)",
        ]
    }

class Notes(DBCore):
    _TABLE = 'notes'
    _CREATE = {
        'sqlite' : [
            "id INTEGER PRIMARY KEY AUTOINCREMENT",
            "date DATETIME DEFAULT CURRENT_TIMESTAMP",
            "eleve_id INTEGER DEFAULT NULL",
            "matiere_id INTEGER DEFAULT NULL",
            "note REAL DEFAULT NULL",

        ],
        'mysql': [
            "id INT NOT NULL AUTO_INCREMENT PRIMARY KEY",
            "date datetime DEFAULT CURRENT_TIMESTAMP",
            "eleve_id INT DEFAULT NULL",
            "matiere_id INT DEFAULT NULL",
            "note FLOAT(5,1) DEFAULT NULL",
            "FOREIGN KEY(eleve_id) REFERENCES eleve(id)",
            "FOREIGN KEY(matiere_id) REFERENCES matiere(id)",
        ]
    }

"""
DB_TABLES = {
    'Classe': Classe,
    'Matiere': Matiere,
    'Eleve': Eleve,
    'Notes': Notes,
}"""

#