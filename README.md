#SqlKit
***
## Description
* Interface python de SGBDR (Système de gestion de base de données relationnelles)
* Package dédié pour mysql et sqlite
* Python 2 et 3
* Simple et intuitif
* Démo sqlkit (usages)

## Installation
* Lancer ./menu_install.sh
* Environnement virtuel python
* Packages: PyMySQL et Faker

## Objectif
* Programmer en python
* Utiliser les bases de données

## Fonctions (APIs)
### Apis fondamentales
* execute(query, p=[])
* executemany(query, p=[])
* executeScript(script)
* commit()
* rollback()
* column_name()
* query(query, n='all', p=[])

### Apis courantes
* all(whereclause="")
* get_by_id(self, pkey, key='id')
* exists(pkey, key='id')
* add(**params)
* upd(whereclause='', **params)
* rem(whereclause='')
* first_row(pid='id')
* first_row_id(pid='id')
* last_row(pid='id')
* last_row_id(pid='id')

## Usages

* Créer une table SQL
* Insérer des données
* Sélectionner des données
* Modifier des données
* Supprimer des données
* Lancer la démo: ./sklkit.py


(c) denis@e-educ.fr
