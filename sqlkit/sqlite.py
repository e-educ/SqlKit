# -*- coding: UTF-8 -*-
#
# @author: denis  <webmaster@vigiapis.fr> <denis.defolie@e-educ.fr>'
import sqlite3
from .com import SQLCommon


class SqliteEngine(object):
    def __init__(self, dbname):
        super(SqliteEngine, self).__init__()
        self.connection = sqlite3.connect(dbname, check_same_thread=False)
        self.database = self.connection.cursor()


class DBSqlite(SQLCommon):
    def __init__(self, engine):
        super(DBSqlite, self).__init__(engine)
        self.cnx= engine.connection
        self.db = engine.database

    def insert(self, table, **params):
        dataname, valuename, values = [], [], []
        for k, v in params.items():
            v = str(v)
            dataname.append(k)
            if v.upper()=='NULL' or v=='':
                valuename.append('NULL')
            else:
                valuename.append('?')
                values.append(v)
        query = "insert into %s (%s) values (%s)"% (table, ','.join(dataname), ','.join(valuename))
        return self.execute(query, values)

    def update(self, table, whereclause='', **params):
        valuename, values = [], []
        for k, v in params.items():
            valuename.append('%s=%s'%(k, '?'))
            values.append(str(v))
        query = "update %s set %s %s"% (table, ','.join(valuename), whereclause)
        return self.execute(query, values)

    def delete(self, table, whereclause=''):
        return self.db.execute("delete from %s %s"% (table, whereclause), [])

    def tableCreate(self, table, query):
        self.tableDrop(table)
        self.execute("create table  if not exists %s (%s)" % (table, query), [])

    def tableDrop(self, table):
        self.execute("drop table if exists %s" % (table), [])

#