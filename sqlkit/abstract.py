# encoding: utf-8
# abstract
#
# Created on 5 juin 2017
#
# @author: denis
#
from app.settings import database_conf

base = database_conf.get('default')
mysql_conf = database_conf.get('mysql')
sqlite_conf = database_conf.get('sqlite')

try:
    if base == 'mysql':
        from .mysql import DBMysql
        class DBsql(DBMysql):
            def __init__(self):
                self.mod = base
                super(DBsql, self).__init__(**mysql_conf)
    elif base == 'sqlite':
        from .sqlite import DBSqlite, SqliteEngine
        engine = SqliteEngine(sqlite_conf)
        class DBsql(DBSqlite):
            def __init__(self):
                self.mod = base
                super(DBsql, self).__init__(engine)
    else:
        raise Exception('Not a valid database!...')
except Exception as e:
    print('BaseSQL error: ', e)
    exit(0)


class DBCore(DBsql):
    _TABLE = 'table'
    _CREATE = {
        'sqlite' : [
            "id INTEGER PRIMARY KEY AUTOINCREMENT",
        ],
        'mysql': [
            "id INT NOT NULL AUTO_INCREMENT PRIMARY KEY",
        ]
    }
    _CONSTRAINT = ''
    _INDEX = ''

    def __init__(self):
        super(DBCore, self).__init__()
        self.table = self._TABLE
        self.create_tbl(self._CREATE.get(self.mod))
        self.init()

    def init(self):
        pass

    def create_tbl(self, query):
        try:
            if self.last_row_id() > 0:
                return True
        except Exception as e:
            self.tableCreate(self.table, ','.join(query))
            return True

    def all(self, whereclause=""):
        return self.query("select * from %s %s" % (self.table, whereclause))

    def get_by_id(self, pkey, key='id'):
        r = self.query("select * from %s where %s='%s'" % (self.table, key, pkey, ), n='one')
        if r:
            return r[0]
        return None

    def exists(self, pkey, key='id'):
        r = self.get_by_id(pkey, key)
        return True if r else False

    def add(self, **params):
        return self.insert(self.table, **params)

    def upd(self, whereclause='', **params):
        return self.update(self.table, whereclause, **params)

    def rem(self, whereclause=''):
        return self.delete(self.table, whereclause)

    def first_row(self, pid='id'):
        return self.firstRow(self.table, pid=pid)

    def first_row_id(self, pid='id'):
        return self.firstRowId(self.table, pid=pid)

    def last_row(self, pid='id'):
        return self.lastRow(self.table, pid=pid)

    def last_row_id(self, pid='id'):
        return self.lastRowId(self.table, pid=pid)

#
