# -*- coding: UTF-8 -*-
# Created on   16 mars 2017
#
# @author: denis  <denis.defolie@e-educ.fr>'


class SQLCommon(object):
    def __init__(self, dbname):
        super(SQLCommon, self).__init__()
        self.dbname = dbname
        self.cnx= None
        self.db = None

    def execute(self, q, p=[]):
        if self.db:
            self.db.execute(q, p)
            self.commit()
        return self.db

    def executemany(self, req, p=[]):
        if self.db:
            return self.db.executemany(req, p)

    def executeScript(self, script):
        if self.db:
            return self.db.executescript(script)

    def commit(self):
        if self.cnx:
            self.cnx.commit()

    def rollback(self):
        if self.cnx:
            self.cnx.rollback()

    def firstRow(self, table, pid='id'):
        return self.query("select * from %s order by %s asc" % (table, pid), n='one')

    def firstRowId(self, table, pid='id'):
        r = self.firstRow(table, pid)
        return r[0].get(pid, 0) if r else 0

    def lastRow(self, table, pid='id'):
        return self.query("select * from %s order by %s desc" % (table, pid), n='one')

    def lastRowId(self, table, pid='id'):
        r = self.lastRow(table, pid)
        return r[0].get(pid, 0) if r else 0

    def column_name(self):
        cn = []
        for c in self.db.description:
            cn.append(c[0])
        return cn

    def query(self, q, n='all', p=[]):
        result = []
        self.execute(q, p)
        rows = [self.db.fetchone()] if n=='one' else self.db.fetchall()
        for row in rows:
            d = {}
            if row:
                for i, n in enumerate(self.column_name()):
                    d[n]=row[i]
                result.append(d)
        return result

    @NotImplementedError
    def insert(self, table, **params):
        return self.db

    @NotImplementedError
    def update(self, table, whereclause='', **params):
        return self.db

    @NotImplementedError
    def delete(self, table, whereclause):
        return self.db

    def tableCreate(self, table, query):
        self.execute("create table if not exists %s (%s) DEFAULT CHARSET=UTF8" % (table, query))
        self.commit()

    def tableDrop(self, table):
        self.execute("drop table if exists %s" % (table))
        self.commit()

    def close(self):
        if self.cnx:
            self.cnx.close()

    def __enter__(self):
        return self

    def __exit__(self):
        self.close()
#