# -*- coding: UTF-8 -*-
#
# @author: denis  <webmaster@vigiapis.fr> <denis.defolie@e-educ.fr>'
from warnings import filterwarnings


import pymysql as MySQLdb
from .com import SQLCommon

filterwarnings('ignore', category = MySQLdb.Warning)


class DBMysql(SQLCommon):

    def __init__(self, dbname=None, user=None, passwd=None, host='localhost', port=3306, charset='utf8', use_unicode=True):
        super(DBMysql, self).__init__(dbname)
        self.cnx = MySQLdb.connect(
            host=host,
            port=port,
            user=user,
            passwd=passwd,
            db=dbname,
            charset=charset,
            use_unicode=use_unicode
        )
        #self.cursor = self.cnx.cursor(MySQLdb.cursors.DictCursor)
        self.db = self.cnx.cursor(MySQLdb.cursors.Cursor)

    def insert(self, table, **params):
        dataname, valuename, values = [], [], []
        for k, v in params.items():
            dataname.append(k)
            valuename.append('%s')
            values.append(str(v))
        self.db.execute("lock tables %s write"% (self.table))
        self.db.execute("insert low_priority into %s (%s) values (%s)"%(self.table, ','.join(dataname), ','.join(valuename)), values)
        self.db.execute("unlock tables")
        return self.db

    def update(self, table, whereclause='', **params):
        valuename, values = [], []
        for k, v in params.items():
            valuename.append('%s=%s'%(k, '%s'))
            values.append(str(v))
        self.db.execute("lock tables %s write"% (table))
        self.db.execute("update low_priority %s set %s %s"% (table, ','.join(valuename), whereclause), values)
        self.db.execute("unlock tables")
        return self.db

    def delete(self, table, whereclause=''):
        self.db.execute("lock tables %s write"% (table))
        self.db.execute("delete low_priority from %s %s"% (table, whereclause))

        self.db.execute("unlock tables")
        return self.db

    @staticmethod
    def dbCreate(self, dbname):
        self.execute("create database if not exists %s;" % (dbname))

    @staticmethod
    def dbDrop(self, dbname):
        self.execute("drop database %s;" % (dbname))


#