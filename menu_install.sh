#!/bin/bash
CURRENT="$(pwd)"
cd etc/install

source ./common.conf

SEP="________________________________________________________________________"
C_0="0  - Afficher la documentation README"
C_1="1  - Modifier $BASE/common.conf (IMPERATIF AVANT TOUT)"
C_2="2  - Installation dans les packages systèmes (MODE ROOT)"
C_3="3  - Installation dans un environnement virtuel python"
echo
echo "INSTALLATION DE NOTIFY"
echo "______________________"
echo
echo "  Configuration à modifier si besoin:"
echo "     > $BASE/common.conf."
echo
echo $SEP
echo
echo $C_0
echo $SEP
echo
echo $C_1
echo $C_2
echo $C_3
echo
echo $SEP
echo "Usage: $ $0  <numero>"
echo

if [ $# -ne 1 ]; then
	echo
	echo "Choisir un bon numero" 1>&2
	exit 1
fi


case "$1" in
0)
echo $C_0
cat $CURRENT/README.md | more
;;

1)
echo $C_1
nano ./common.conf
;;

2)
echo $C_2
sudo ./install.sh
;;

3)
echo $C_3
./install-env.sh
;;


esac

exit 0
